package app.checkboard_20190709.db

import app.checkboard_20190709.dto.ReportItemInputData
import io.realm.Realm
import io.realm.RealmResults
import io.realm.Sort
import io.realm.kotlin.createObject
import io.realm.kotlin.where
import java.util.*


object RealmManager {

    private val realm: Realm = Realm.getDefaultInstance()

    /***************************************************************************/
    /* テンプレート */
    /***************************************************************************/

    /**
     * SELECT テンプレート一覧.
     */
    fun selectTemplates(): RealmResults<CBTemplate> {
        return realm
            .where<CBTemplate>()
            .findAll()
            .sort("id", Sort.DESCENDING)
    }

    /**
     * SELECT テンプレート
     */
    fun selectTemplate(templateId: Long): CBTemplate? {
        return realm
            .where<CBTemplate>()
            .equalTo("id", templateId)
            .findFirst()
    }

    /**
     * DELETE テンプレート.
     */
    fun deleteTemplate(templateId: Long) {
        val template = selectTemplate(templateId)
        template?.let {
            realm.executeTransaction {
                val templateItems = selectTemplateItems(templateId)
                templateItems.deleteAllFromRealm()
                template.deleteFromRealm()
            }
        }
    }

    /**
     * REGIST テンプレート.
     */
    fun registTemplate(templateId: Long?, title: String): CBTemplate {

        var template: CBTemplate? = null

        realm.executeTransaction {
            template = findOrCreateTemplate(templateId)
            template!!.title = title
        }

        return selectTemplate(template!!.id) ?: throw IllegalArgumentException()
    }

    /**
     * 取得または生成 テンプレート.
     */
    private fun findOrCreateTemplate(templateId: Long?): CBTemplate {

        return if (templateId == null) {
            val maxId = realm.where<CBTemplate>().max("id")
            val nextId = (maxId?.toLong() ?: 0L) + 1
            realm.createObject(nextId)
        } else {
            val cb: CBTemplate? = selectTemplate(templateId)
            cb?.apply {
                modified_datetime = Date()
            }
            cb!!
        }
    }

    /***************************************************************************/
    /* テンプレート項目 */
    /***************************************************************************/

    /**
     * SELECT テンプレート項目一覧
     */
    private fun selectTemplateItems(templateId: Long): RealmResults<CBTemplateItem> {

        return realm
            .where<CBTemplateItem>()
            .equalTo("id", templateId)
            .findAll()
            .sort("disp_seq", Sort.ASCENDING)
    }

    /**
     * SELECT テンプレート項目
     */
    fun selectTemplateItem(itemId: Long): CBTemplateItem? {
        return realm
            .where<CBTemplateItem>()
            .equalTo("item_id", itemId)
            .findFirst()
    }

    /**
     * DELETE テンプレート項目 削除
     */
    fun deleteTemplateItem(itemId: Long) {
        realm.executeTransaction {
            val item = selectTemplateItem(itemId)
            item?.let {
                var template: CBTemplate? = selectTemplate(item.template_id)
                template?.apply {  modified_datetime = Date() }

                val items = realm
                    .where<CBTemplateItem>()
                    .equalTo("template_id", item.template_id)
                    .greaterThan("disp_seq", item.disp_seq)
                    .findAll()

                for (i in items) {
                    i.disp_seq = i.disp_seq - 1
                }

                item.deleteFromRealm()
            }
        }
    }

    /**
     * UPDATE テンプレート項目 表示順更新.
     */
    fun updateTemplateItemDispSeq(itemId: Long, dispSeq: Int): CBTemplateItem {

        realm.executeTransaction {
            val item = selectTemplateItem(itemId)
            item?.let {
                item.disp_seq = dispSeq

                var template: CBTemplate? = selectTemplate(item.template_id)
                template?.apply {  modified_datetime = Date() }
            }
        }

        return selectTemplateItem(itemId) ?: throw IllegalArgumentException()
    }

    /**
     * REGIST テンプレート項目.
     */
    fun registTemplateItem(
        templateId: Long, itemId: Long?,
        title: String, qty: Float): CBTemplateItem {

        var item: CBTemplateItem? = null

        realm.executeTransaction {
            item = findOrCreateTemplateItem(templateId, itemId)
            item?.let {
                it.title = title
                it.quantity = qty
            }

            var template: CBTemplate? = selectTemplate(templateId)
            template?.apply {  modified_datetime = Date() }
        }

        return selectTemplateItem(item!!.item_id) ?: throw IllegalArgumentException()
    }

    /**
     * 取得 or 生成 テンプレート項目.
     */
    private fun findOrCreateTemplateItem(
        templateId: Long, itemId: Long?): CBTemplateItem {

        return when (itemId) {
            null -> {
                val maxId = realm
                    .where<CBTemplateItem>()
                    .max("itemId")
                val nextId = (maxId?.toLong() ?: 0L) + 1

                val maxDispSeq = realm
                    .where<CBTemplateItem>()
                    .equalTo("templateId", templateId)
                    .max("disp_seq")
                val nextDispSeq = (maxDispSeq?.toInt() ?: 0) + 1

                val item: CBTemplateItem? = realm.createObject(nextId)
                item?.apply {
                    this.template_id = templateId
                    this.disp_seq = nextDispSeq
                }
            }
            else -> {
                selectTemplateItem(itemId)
            }
        } ?: throw IllegalArgumentException()
    }


    /***************************************************************************/
    /* レポート*/
    /***************************************************************************/

    /**
     * SELECT レポート 一覧
     */
    fun selectReports(): RealmResults<CBReport> {
        return realm
            .where<CBReport>()
            .findAll()
            .sort("id", Sort.DESCENDING)
    }

    /**
     * SELECT レポート
     */
    fun selectReport(reportId: Long): CBReport? {
        return realm
            .where<CBReport>()
            .equalTo("id", reportId)
            .findFirst()
    }

    /**
     * DELETE レポート
     */
    fun deleteReport(reportId: Long) {
        val rep = selectReport(reportId)
        rep?.let {
            realm.executeTransaction {
                val items = selectReportItems(reportId)
                items.deleteAllFromRealm()
                rep.deleteFromRealm()
            }
        }
    }

    /**
     * REGIST レポート.
     */
    fun registReport(
        reportId: Long?,
        title: String, groupId: Long?): CBReport {

        var rep: CBReport? = null

        realm.executeTransaction {
            rep = findOrCreateReport(reportId)
            rep?.let {
                it.title = title
                it.group_id = groupId
            }
        }

        return selectReport(rep!!.id) ?: throw IllegalArgumentException()
    }

    /**
     * 取得または生成 レポート.
     */
    private fun findOrCreateReport(reportId: Long?): CBReport {

        return when (reportId) {
            null -> {
                val maxId = realm.where<CBReport>().max("id")
                val nextId = (maxId?.toLong() ?: 0L) + 1
                realm.createObject(nextId)
            }
            else -> {
                val rep = selectReport(reportId)
                rep?.apply {  modified_datetime = Date() }
            }
        } ?: throw IllegalArgumentException()
    }


    /***************************************************************************/
    /* レポート項目 */
    /***************************************************************************/

    /**
     * SELECT レポート項目 一覧
     */
    fun selectReportItems(reportId: Long): RealmResults<CBReportItem> {

        return realm
            .where<CBReportItem>()
            .equalTo("report_id", reportId)
            .findAll()
            .sort("disp_seq", Sort.ASCENDING)
    }

    /**
     * SELECT レポート項目
     */
    private fun selectReportItem(itemId: Long): CBReportItem? {
        return realm
            .where<CBReportItem>()
            .equalTo("item_id", itemId)
            .findFirst()
    }

    /**
     * UPDATE レポート項目 チェック状態更新.
     */
    fun updateReportItemCheckedState(itemId: Long, checked: Boolean): CBReportItem {

        realm.executeTransaction {
            val item = selectReportItem(itemId)
            item?.let {
                item.checked = checked

                var rep: CBReport? = selectReport(item.report_id)
                rep?.apply {  modified_datetime = Date() }
            }
        }

        return selectReportItem(itemId) ?: throw IllegalArgumentException()
    }

    /**
     * UPDATE レポート項目 表示順更新.
     */
    fun updateReportItemDispSeq(itemId: Long, dispSeq: Int): CBReportItem {

        realm.executeTransaction {
            val item = selectReportItem(itemId)
            item?.let {
                item.disp_seq = dispSeq

                var report: CBReport? = selectReport(item.report_id)
                report?.apply {  modified_datetime = Date() }
            }
        }

        return selectReportItem(itemId) ?: throw IllegalArgumentException()
    }

    /**
     * DELETE レポート項目
     */
    fun deleteReportItem(itemId: Long) {
        realm.executeTransaction {
            val item = selectReportItem(itemId)
            item?.let {
                var report: CBReport? = selectReport(item.report_id)
                report?.apply {  modified_datetime = Date() }

                val items = realm
                    .where<CBReportItem>()
                    .equalTo("item_id", item.item_id)
                    .greaterThan("disp_seq", item.disp_seq)
                    .findAll()

                for (i in items) {
                    i.disp_seq = i.disp_seq - 1
                }

                item.deleteFromRealm()
            }
        }
    }

    /**
     * REGIST レポート項目.
     */
    fun registReportItem(
        reportId: Long, itemId: Long?,
        inputData: ReportItemInputData
    ): CBReportItem {

        var item: CBReportItem? = null

        realm.executeTransaction {
            item = findOrCreateReportItem(reportId, itemId)
            item?.let {
                it.title = inputData.title
                it.quantity = inputData.qtyValue
                it.actual_quantity = inputData.actualQtyValue
                it.memo = inputData.memo
            }

            var report = selectReport(reportId)
            report?.apply {  modified_datetime = Date() }
        }

        return selectReportItem(item!!.item_id) ?: throw IllegalArgumentException()
    }

    /**
     * 取得 or 生成 レポート項目.
     */
    private fun findOrCreateReportItem(
        reportId: Long, itemId: Long?): CBReportItem {

        return when (itemId) {
            null -> {
                val maxId = realm
                    .where<CBReportItem>()
                    .max("item_id")
                val nextId = (maxId?.toLong() ?: 0L) + 1

                val maxDispSeq = realm
                    .where<CBReportItem>()
                    .equalTo("report_id", reportId)
                    .max("disp_seq")
                val nextDispSeq = (maxDispSeq?.toInt() ?: 0) + 1

                val item: CBReportItem? = realm.createObject(nextId)
                item?.apply {
                    report_id = reportId
                    disp_seq = nextDispSeq
                }
            }
            else -> {
                selectReportItem(itemId)
            }
        } ?: throw IllegalArgumentException()
    }


    /***************************************************************************/
    /* 要素グループ */
    /***************************************************************************/

    /**
     * SELECT 要素グループ 一覧
     */
    fun selectElementGroups(): RealmResults<ElementGroup> {
        return realm
            .where<ElementGroup>()
            .findAll()
            .sort("disp_seq", Sort.ASCENDING)
    }

    /**
     * SELECT 要素グループ
     */
    fun selectElementGroup(groupId: Long): ElementGroup? {
        return realm
            .where<ElementGroup>()
            .equalTo("id", groupId)
            .findFirst()
    }

    /**
     * UPDATE 要素グループ 表示順更新.
     */
    fun updateElementGroupDispSeq(groupId: Long, dispSeq: Int): ElementGroup {

        realm.executeTransaction {
            val item = selectElementGroup(groupId)
            item?.let {
                item.disp_seq = dispSeq
            }
        }

        return selectElementGroup(groupId) ?: throw IllegalArgumentException()
    }

    /**
     * Delete 要素グループ
     */
    fun deleteElementGroup(groupId: Long) {
        val eleGrp = selectElementGroup(groupId)
        eleGrp?.let {
            realm.executeTransaction {
                //                val checkReportItems = selectCheckReportItems(checkReportId)
//                checkReportItems.deleteAllFromRealm()
                eleGrp.deleteFromRealm()
            }
        }
    }

    /**
     * 登録 要素グループ
     */
    fun registElementGroup(groupId: Long?, title: String): ElementGroup {

        var eleGrp: ElementGroup? = null

        realm.executeTransaction {
            eleGrp = findOrCreateElementGroup(groupId)
            eleGrp!!.title = title
        }

        return selectElementGroup(eleGrp!!.id) ?: throw IllegalArgumentException()
    }

    /**
     * 取得または生成 要素グループ.
     */
    private fun findOrCreateElementGroup(groupId: Long?): ElementGroup {

        return when (groupId) {
            null -> {
                val maxId = realm.where<ElementGroup>().max("id")
                val nextId = (maxId?.toLong() ?: 0L) + 1

                val maxDispSeq = realm
                    .where<ElementGroup>()
                    .max("disp_seq")
                val nextDispSeq = (maxDispSeq?.toInt() ?: 0) + 1

                val createdGrp: ElementGroup? = realm.createObject(nextId)
                createdGrp?.apply { disp_seq = nextDispSeq }
            }
            else -> {
                selectElementGroup(groupId)
            }
        } ?: throw IllegalArgumentException()
    }
}