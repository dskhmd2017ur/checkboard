package app.checkboard_20190709.db

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class CBTemplateItem : RealmObject() {

    @PrimaryKey
    var item_id: Long = 0

    var template_id: Long = 0

    var title: String = ""
    var quantity: Float = 0.0F

    var disp_seq: Int = 0

}