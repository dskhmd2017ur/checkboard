package app.checkboard_20190709.db

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import app.checkboard_20190709.util.formatDatetime01

import java.util.*

open class CBTemplate : RealmObject() {

    @PrimaryKey
    var id: Long = 0

    var title: String = ""
    var created_datetime: Date = Date()
    var modified_datetime: Date? = null

    val metaInfo: String
        get() {
            val f1 = formatDatetime01(this.created_datetime)
            val f2 = when {
                this.modified_datetime != null -> formatDatetime01(this.modified_datetime)
                else -> "　‐　"
            }
            return "$f1　[修正：$f2]"
        }
}