package app.checkboard_20190709.db

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import app.checkboard_20190709.util.formatDatetime01

open class CBReportItem : RealmObject() {

    @PrimaryKey
    var item_id: Long = 0

    var report_id: Long = 0

    var title: String = ""
    var quantity: Float = 0.0F

    var disp_seq: Int = 0

    var checked: Boolean = false
    var actual_quantity: Float = 0.0F

    var memo: String = ""

    val hasMemo: Boolean
        get() {
            return this.memo != ""
        }

    val quantityText: String
        get() {
            return if (this.quantity == 0.0F && this.actual_quantity == 0.0F) {
                ""
            } else {
                "数量：${this.actual_quantity} ／ ${this.quantity}"
            }
        }
}