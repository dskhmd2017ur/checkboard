package app.checkboard_20190709.db


import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class ElementGroup : RealmObject() {

    @PrimaryKey
    var id: Long = 0

    var title: String = ""

    var disp_seq: Int = 0
}