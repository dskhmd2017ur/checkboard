package app.checkboard_20190709.util

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun String.toDate(pattern: String = "yyyy/MM/dd HH:mm"): Date? {

    val sdFormat = SimpleDateFormat(pattern)

    return sdFormat?.let {

        try {
            it.parse(this)
        } catch (e: ParseException) {
            null
        }
    }
}