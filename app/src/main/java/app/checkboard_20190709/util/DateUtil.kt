package app.checkboard_20190709.util

import android.text.format.DateFormat
import java.util.*

fun formatDatetime01(d: Date?) : CharSequence {
    if (d == null) return ""
    return DateFormat.format("yyyy/MM/dd (E) HH:mm", d)
}