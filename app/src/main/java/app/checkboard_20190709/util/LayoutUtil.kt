package app.checkboard_20190709.util

import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable

fun getBorders(
    bgColor: Int, borderColor: Int,
    left: Int, top: Int, right: Int, bottom: Int): LayerDrawable {

    // Initialize new color drawables
    val borderColorDrawable = ColorDrawable(borderColor)
    val backgroundColorDrawable = ColorDrawable(bgColor)

    // Initialize a new array of drawable objects
    val drawables = arrayOf<Drawable>(borderColorDrawable, backgroundColorDrawable)

    // Initialize a new layer drawable instance from drawables array
    val layerDrawable = LayerDrawable(drawables)

    // Set padding for background color layer
    layerDrawable.setLayerInset(
        1, // Index of the drawable to adjust [background color layer]
        left, // Number of pixels to add to the left bound [left border]
        top, // Number of pixels to add to the top bound [top border]
        right, // Number of pixels to add to the right bound [right border]
        bottom // Number of pixels to add to the bottom bound [bottom border]
    )

    // Finally, return the one or more sided bordered background drawable
    return layerDrawable
}