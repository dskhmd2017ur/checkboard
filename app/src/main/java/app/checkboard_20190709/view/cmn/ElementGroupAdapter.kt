package app.checkboard_20190709.view.cmn

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import app.checkboard_20190709.util.getBorders

const val ELE_GROUP_CODE_NOT_SELECTED = "NotSelected"
const val ELE_GROUP_CODE_NEW_ITEM = "NewItem"


class ElementGroupItem(
    var code: String,
    var Text: String) {

    var isNone: Boolean = false
    var isNew: Boolean = false

    init {
        when (code) {
            ELE_GROUP_CODE_NOT_SELECTED -> {
                isNone = true
            }
            ELE_GROUP_CODE_NEW_ITEM -> {
                isNew = true
            }
        }
    }
}

class ElementGroupAdapter : ArrayAdapter<ElementGroupItem> {

//    constructor(context: Context) : super(context, android.R.layout.simple_spinner_item) {
//        setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//    }



    constructor(context: Context, list: MutableList<ElementGroupItem>) :
            super(context, android.R.layout.simple_spinner_item) {

        this.addAll(
            ElementGroupItem(ELE_GROUP_CODE_NOT_SELECTED, "グループを選択…"),
            ElementGroupItem(ELE_GROUP_CODE_NEW_ITEM, "新しいグループ…")
        )

        this.addAll(list)


        setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    }

    override fun getView(
        position: Int, convertView: View?, parent: ViewGroup): View {

        val textView = super.getView(position, convertView, parent) as TextView
        textView.text = getItem(position)!!.Text
        return textView
    }

    override fun getDropDownView(
        position: Int, convertView: View?, parent: ViewGroup): View {

        val textView = super.getDropDownView(position, convertView, parent) as TextView

        textView.background = when (position) {
            1 -> {
                //val gd = GradientDrawable()
                //gd.setColor(-0xff0100) // Changes this drawbale to use a single color instead of a gradient
                //gd.cornerRadius = 5f
                //gd.setStroke(1, -0x1000000)
                //gd

                // Generate bottom border only

                getBorders(
                    Color.WHITE, // Background color
                    Color.LTGRAY, // Border color
                    0, // Left border in pixels
                    0, // Top border in pixels
                    0, // Right border in pixels
                    10 // Bottom border in pixels
                )
            }
            else -> null
        }

        textView.text = getItem(position)!!.Text
        return textView
    }
}