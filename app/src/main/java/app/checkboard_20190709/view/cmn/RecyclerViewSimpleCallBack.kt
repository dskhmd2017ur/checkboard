package app.checkboard_20190709.view.cmn

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log


open class RecyclerViewSimpleCallBack<T>(
    protected val recyclerView: RecyclerView,
    protected  val dataSet: MutableList<T>)
        : ItemTouchHelper.SimpleCallback(
    ItemTouchHelper.UP or ItemTouchHelper.DOWN, ItemTouchHelper.LEFT) {

    interface OnDraggingListener {
        //fun onStartDragging(viewHolder : RecyclerView.ViewHolder)
        fun onEndDragging(viewHolder : RecyclerView.ViewHolder)
        fun onMoveDragging(from: RecyclerView.ViewHolder, to: RecyclerView.ViewHolder)
    }

    private var _onDraggingListener: OnDraggingListener? = null

    fun setOnDraggingListener(listener: OnDraggingListener) {
        _onDraggingListener = listener
    }

    // ドラッグしたとき
    override fun onMove(
        p0: RecyclerView,
        p1: RecyclerView.ViewHolder,
        p2: RecyclerView.ViewHolder): Boolean {

        val fromPosition = p1.adapterPosition
        val toPosition = p2.adapterPosition

        _onDraggingListener?.onMoveDragging(p1, p2)

        /*
        * ドラッグ時、viewType が異なるアイテムを超えるときに、
        * notifyItemMoved を呼び出すと、ドラッグ操作がキャンセルされてしまう。
        * （ドラッグは同じviewTypeを持つアイテム間で行う必要がある模様）
        *
        * 同じ ViewType アイテムを超える時だけ notifyItemMoved を呼び出す。
        * */
        if (p1.itemViewType == p2.itemViewType) {
            dataSet.add(toPosition, dataSet.removeAt(fromPosition))
            recyclerView.adapter.notifyItemMoved(fromPosition, toPosition)
        }

        //Log.d("onMove", fromPosition.toString() + "," + toPosition.toString())

        return true
    }

    override fun clearView(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder) {

        super.clearView(recyclerView, viewHolder)
        _onDraggingListener?.onEndDragging(viewHolder)
    }

    //左にスワイプしたとき
    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        viewHolder.let{
            dataSet.removeAt(viewHolder.adapterPosition)
            recyclerView.adapter.notifyItemRemoved(viewHolder.adapterPosition)
        }
    }

    override fun getMoveThreshold(viewHolder: RecyclerView.ViewHolder?): Float {

        //return super.getMoveThreshold(viewHolder)

        return 0.6f //0.4f
    }

    override fun getSwipeThreshold(viewHolder: RecyclerView.ViewHolder?): Float {
        return 0.8f
    }
}