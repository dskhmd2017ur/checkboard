package app.checkboard_20190709.view.cmn

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Spinner
import app.checkboard_20190709.R

import app.checkboard_20190709.db.RealmManager
import app.checkboard_20190709.dto.ReportItemInputData
import app.checkboard_20190709.dto.TitleAndGroupInputData





interface IAlertDialogCreateParam

abstract class AlertDialogCreateParam : IAlertDialogCreateParam {

    var title: String = ""
    var message: String = ""
}

open class OneButtonDialogCreateParam : AlertDialogCreateParam() {
    var positiveSetting: Pair<String, DialogInterface.OnClickListener>? = null
}

open class TwoButtonDialogCreateParam : AlertDialogCreateParam() {
    var positiveSetting: Pair<String, DialogInterface.OnClickListener>? = null
    var negativeSetting: Pair<String, DialogInterface.OnClickListener>? = null
}

class DeleteConfirmDialogCreateParam: TwoButtonDialogCreateParam() {
    init {
        title = "Delete OK?"
    }

    fun setOnPositiveClickListener(l: DialogInterface.OnClickListener) {
        positiveSetting = Pair("OK", l)
    }

    fun setOnNegativeClickListener(l: DialogInterface.OnClickListener) {
        negativeSetting = Pair("Cancel", l)
    }
}

class AlertDialogFragment : DialogFragment() {

    var dialogCreateParam: IAlertDialogCreateParam? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return when (dialogCreateParam) {
            is TwoButtonDialogCreateParam -> createTwoButtonDialog(
                dialogCreateParam as TwoButtonDialogCreateParam)
            is OneButtonDialogCreateParam -> createOneButtonDialog(
                dialogCreateParam as OneButtonDialogCreateParam)
            else -> createDummyDialog()
        }
    }

    private fun createOneButtonDialog(param: OneButtonDialogCreateParam): AlertDialog {
        return AlertDialog.Builder(activity)
            .setTitle(param.title)
            .setMessage(param.message)
            .setPositiveButton(param.positiveSetting?.first ?: "OK",  param.positiveSetting?.second)
            .create()
    }

    private fun createTwoButtonDialog(param: TwoButtonDialogCreateParam): AlertDialog {
        return AlertDialog.Builder(activity)
            .setTitle(param.title)
            .setMessage(param.message)
            .setPositiveButton(param.positiveSetting?.first ?: "OK",  param.positiveSetting?.second)
            .setNegativeButton(param.negativeSetting?.first ?: "Cancel",  param.negativeSetting?.second)
            .create()
    }

    private fun createDummyDialog(): AlertDialog {
        return AlertDialog.Builder(activity)
            .setTitle("タイトル")
            .setMessage("メッセージ")
            .create()
    }

    override fun onPause() {
        super.onPause()
        // onPause でダイアログを閉じる場合
        dismiss()
    }
}



/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


/**
 * タイトル編集ダイアログ.
 */
class TitleEditDialog : DialogFragment()  {

    interface OnClickListener {
        fun onPositiveClick(inputTitle: String)
        fun onNegativeClick()
    }

    private lateinit var _titleEditText: EditText
    private var _listener: OnClickListener? = null

    fun setOnClickLister(listener: OnClickListener) {
        _listener = listener
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        if (context == null) return super.onCreateDialog(savedInstanceState)

        val builder = AlertDialog.Builder(context).apply {

            // カスタムビューを設定
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val layout = inflater.inflate(R.layout.content_edit_title, null)

            _titleEditText = layout.findViewById(R.id.txt_title)

            arguments?.let {
                // タイトル.
                _titleEditText.setText( it.getString("arg_title"))
            }

            setView(layout)

            // OKボタン
            setPositiveButton("OK") { dialog, which ->
                var input = _titleEditText.text.toString()
                _listener?.onPositiveClick(input)
            }

            // Cancelイベント
            setNegativeButton("Cancel") { dialog, which ->
                _listener?.onNegativeClick()
            }
        }

        return builder.create()
    }
}



/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/



/**
 * タイトル・グループ編集ダイアログ.
 */
class TitleAndGroupEditDialog : DialogFragment()  {

    interface OnClickListener {
        fun onPositiveClick(inputData: TitleAndGroupInputData)
        fun onNegativeClick()
    }

    private lateinit var _titleEditText: EditText
    private lateinit var _groupSpinner: Spinner
    private lateinit var _groupEditText: EditText

    private var _listener: OnClickListener? = null

    fun setOnClickLister(listener: OnClickListener) {
        _listener = listener
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        if (context == null) return super.onCreateDialog(savedInstanceState)

        val builder = AlertDialog.Builder(context).apply {

            // カスタムビューを設定
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val layout = inflater.inflate(app.checkboard_20190709.R.layout.content_edit_title_group, null)

            _titleEditText = layout.findViewById(app.checkboard_20190709.R.id.txt_title)
            _groupEditText = layout.findViewById(app.checkboard_20190709.R.id.txt_grp_title)
            _groupSpinner = layout.findViewById(app.checkboard_20190709.R.id.spnr_item_grp)

            // グループ.
            var eleGrps = RealmManager.selectElementGroups().toMutableList()
            val spinnerItems = mutableListOf<ElementGroupItem>()
            eleGrps.forEach {
                spinnerItems.add(ElementGroupItem(it.id.toString(), it.title))
            }

            val adapter = ElementGroupAdapter(context, spinnerItems)
            _groupSpinner.adapter = adapter
            _groupSpinner.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener{
                    override fun onItemSelected(
                        parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                        val spinnerParent = parent as Spinner
                        val item = spinnerParent.selectedItem as ElementGroupItem

                        if (item.isNew) {
                            _groupEditText.visibility = View.VISIBLE
                            _groupEditText.requestFocus()
                        } else {
                            _groupEditText.setText("")
                            _groupEditText.visibility = View.INVISIBLE
                        }
                    }
                    //　アイテムが選択されなかった
                    override fun onNothingSelected(parent: AdapterView<*>?) { }
                }

            arguments?.let {
                // タイトル.
                _titleEditText.setText( it.getString("arg_title"))
                if (it.containsKey("arg_group_id")) {
                    val groupId = it.getLong("arg_group_id").toString()
                    val g = spinnerItems.singleOrNull {  x -> x.code == groupId }
                    if (g != null) _groupSpinner.setSelection(adapter.getPosition(g))
                }
            }

            setView(layout)

            // OKボタン
            setPositiveButton("OK") { dialog, which ->
                val inputData = TitleAndGroupInputData()
                inputData.title = _titleEditText.text.toString()
                inputData.newGroupTitle = _groupEditText.text.toString()
                inputData.groupItem = _groupSpinner.selectedItem as ElementGroupItem

                _listener?.onPositiveClick(inputData)
            }

            // Cancelイベント
            setNegativeButton("Cancel") { dialog, which ->
                _listener?.onNegativeClick()
            }
        }

        return builder.create()
    }
}



/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/**
 * レポート項目 編集ダイアログ.
 */
class ReportItemEditDialog : DialogFragment()  {

    interface OnClickListener {
        fun onPositiveClick(inputData: ReportItemInputData)
        fun onNegativeClick()
        fun onDeleteClick(reportItemId: Long?)
    }

    private var _titleEditText: EditText? = null
    private var _qtyEditText: EditText? = null
    private var _actualQtyEditText: EditText? = null
    private var _checkReportItemId: Long? = null
    private var _memoEditText: EditText? = null

    private var _listener: OnClickListener? = null

    fun setOnClickLister(listener: OnClickListener) {
        _listener = listener
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        if (context == null) return super.onCreateDialog(savedInstanceState)

        val builder = AlertDialog.Builder(context).apply {

            // カスタムビューを設定
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val layout = inflater.inflate(app.checkboard_20190709.R.layout.content_edit_report_item, null)

            _titleEditText = layout.findViewById(app.checkboard_20190709.R.id.txt_report_item_title)
            _titleEditText?.let {
                it.setText( arguments?.getString("arg_item_title"))
                it.setSelection(it.text.length)
            }

            _qtyEditText = layout.findViewById(app.checkboard_20190709.R.id.txt_report_item_qty)
            _qtyEditText?.let {
                it.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(5, 1))
                it.setText( (arguments?.getFloat("arg_item_qty", 0F) ?: 0F).toString())
                it.setSelection(it.text.length)
            }

            _actualQtyEditText = layout.findViewById(app.checkboard_20190709.R.id.txt_report_item_actual_qty)
            _actualQtyEditText?.let {
                it.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(5, 1))
                it.setText( (arguments?.getFloat("arg_item_actual_qty", 0F) ?: 0F).toString())
                it.setSelection(it.text.length)
            }

            _memoEditText = layout.findViewById(app.checkboard_20190709.R.id.txt_report_item_memo)
            _memoEditText?.let {
                it.setText( arguments?.getString("arg_item_memo"))
                it.setSelection(it.text.length)
            }

            val delBtn: View? = layout.findViewById(app.checkboard_20190709.R.id.img_del)
            val itemId = arguments?.getLong("arg_item_id", 0L) ?: 0L

            when  {
                0L < itemId -> {
                    _checkReportItemId = itemId
                    delBtn?.let {
                        it.setOnClickListener {
                            _listener?.onDeleteClick(_checkReportItemId)
                            dismiss()
                        }
                    }
                }
                else -> {
                    _checkReportItemId = null
                    delBtn?.visibility = View.INVISIBLE
                }
            }

            setView(layout)

            setPositiveButton("OK") { dialog, which ->
                val inputData = ReportItemInputData()
                inputData.title = _titleEditText?.text.toString()
                inputData.qty = _qtyEditText?.editableText.toString()
                inputData.actualQty = _actualQtyEditText?.editableText.toString()
                inputData.memo = _memoEditText?.text.toString()

                _listener?.onPositiveClick(inputData)
            }

            setNegativeButton("Cancel") { dialog, which ->
                _listener?.onNegativeClick()
            }
        }

        return builder.create()
    }
}