package app.checkboard_20190709.view.cmn

import android.app.Activity
import android.content.Context
import android.graphics.Rect
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.view.ViewTreeObserver
import android.view.inputmethod.InputMethodManager
import app.checkboard_20190709.view.main.OnKeyboardVisibilityListener


fun Vibe(activity: Activity) {

      // ドラッグ開始通知のためにバイブさせる場合。API レベルによって使用できるメソッドが異なるので分岐
      val vibrator = activity.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
      if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
          val vibrationEffect = VibrationEffect.createOneShot(
              300, VibrationEffect.DEFAULT_AMPLITUDE)
          //vibrator.vibrate(vibrationEffect)
      } else {
          //vibrator.vibrate(300)
      }
}

/**
 * Soft input area be shown to the user.
 *
 * @param context The context to use
 * @param view The currently focused view, which would like to receive soft keyboard input
 * @return Result
 */
fun showSoftInput(context: Context, view: View): Boolean {
    return showSoftInput(context, view, 0)
}

/**
 * Soft input area be shown to the user.
 *
 * @param context The context to use
 * @param view The currently focused view, which would like to receive soft keyboard input
 * @param flags Provides additional operating flags
 * @return Result
 */
fun showSoftInput(context: Context, view: View, flags: Int): Boolean {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    return imm.showSoftInput(view, flags)
}

fun setKeyboardVisibilityListener(
    parentView: View,
    onKeyboardVisibilityListener: OnKeyboardVisibilityListener
) {
    //val parentView = currentView.findViewById(R.id.webview) as ViewGroup//.getChildAt(0);

    //val parentView = search_view_layout

    parentView.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        private var alreadyOpen: Boolean = false
        private val defaultKeyboardHeightDP = 100
        private val EstimatedKeyboardDP =
            defaultKeyboardHeightDP + if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) 48 else 0
        private val rect = Rect()
        override fun onGlobalLayout() {
            val estimatedKeyboardHeight = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                EstimatedKeyboardDP.toFloat(),
                parentView.resources.displayMetrics
            ).toInt()
            parentView.getWindowVisibleDisplayFrame(rect)
            val heightDiff = parentView.rootView.height - (rect.bottom - rect.top)
            val isShown = heightDiff >= estimatedKeyboardHeight
            if (isShown == alreadyOpen) {
                Log.i("Keyboard state", "Ignoring global layout change...")
                return
            }
            alreadyOpen = isShown
            onKeyboardVisibilityListener.onVisibilityChanged(isShown)
        }
    })
}