package app.checkboard_20190709.view.cmn

import android.support.v4.app.FragmentManager
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity


const val PAGE_IDX_LIST = 0
const val PAGE_IDX_EDIT = 1

interface ListEditPageEventListener :
    ListEditPageAdapterEventListener,
    BaseListEditPageController.OnPageSelectedListener

/**
 * 基底 リスト・編集 ページコントローラ.
 */
abstract class BaseListEditPageController(
    protected val pager: ViewPager,
    val fragmentManager: FragmentManager,
    protected val activity: AppCompatActivity) {

    private  lateinit var  _pageAdapter: BaseListEditPageAdapter

    protected val pageAdapter: BaseListEditPageAdapter
        get() = _pageAdapter

    protected val listPageFragment: BaseListPageFragment
        get() = _pageAdapter.listPageFragment

    protected val editPageFragment: BaseEditPageFragment
        get() = _pageAdapter.editPageFragment

    interface OnPageSelectedListener {
        fun onListPageSelected()
        fun onEditPageSelected()
    }

    private var _listener: ListEditPageEventListener? = null

    protected fun setListEditPageEventListener(listener: ListEditPageEventListener?) {
        _listener = listener
    }

    fun initialize() {
        _pageAdapter = createPageAdapter()

        _listener?.let {
            _pageAdapter.setEventListener(_listener as ListEditPageAdapterEventListener)
        }

        pager.adapter = _pageAdapter

        pager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {

                if (_listener == null) return

                val l = _listener as OnPageSelectedListener

                when (position) {
                    PAGE_IDX_LIST -> l.onListPageSelected()
                    PAGE_IDX_EDIT -> l.onEditPageSelected()
                }
            }
        })
    }

    val isListPageSelected: Boolean
        get() = pager.currentItem == PAGE_IDX_LIST


    val isEditPageSelected: Boolean
        get() =  pager.currentItem == PAGE_IDX_EDIT


    fun selectListPage() {
        pager.currentItem = PAGE_IDX_LIST
    }

    fun selectEditPage() {
        pager.currentItem = PAGE_IDX_EDIT
    }
    
    //********************************************************

    abstract fun createPageAdapter(): BaseListEditPageAdapter
}
