package app.checkboard_20190709.view.cmn

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.checkboard_20190709.R
import kotlinx.android.synthetic.main.content_list.*


/**
 * FragmentViewCreated リスナ.
 */
interface OnFragmentViewCreatedListener {
    fun onFragmentViewCreated(
        fragment: Fragment,
        view: View,
        savedInstanceState: Bundle?)
}

/**
 * リスト・編集ページアダプタ　イベントリスナ.
 */
interface ListEditPageAdapterEventListener :
    OnFragmentViewCreatedListener


/******************************************************************************************************/
// ページアダプタ関連
/******************************************************************************************************/


/**
 * リスト・編集ページアダプタ.
 */
abstract class BaseListEditPageAdapter (
    private val pageController: BaseListEditPageController,
    fm : FragmentManager) : FragmentPagerAdapter(fm) {

    private lateinit var _listPageFragment: BaseListPageFragment
    private lateinit var _editPageFragment: BaseEditPageFragment

    val listPageFragment: BaseListPageFragment
        get() = _listPageFragment

    val editPageFragment: BaseEditPageFragment
        get() = _editPageFragment

    protected var listener: ListEditPageAdapterEventListener? = null

    fun setEventListener(l: ListEditPageAdapterEventListener) {
        listener = l
    }

    // 各ページの情報
    override fun getItem(position: Int): Fragment {

        return when (position)  {
            PAGE_IDX_LIST -> {
                _listPageFragment = createListPageFragment().apply {
                    this.setOnFragmentViewCreatedListener(listener as OnFragmentViewCreatedListener)
                    this.pageController = this@BaseListEditPageAdapter.pageController
                }

                _listPageFragment
            }
            PAGE_IDX_EDIT -> {
                _editPageFragment = createEditPageFragment().apply {
                    this.setOnFragmentViewCreatedListener(listener as OnFragmentViewCreatedListener)
                    this.pageController = this@BaseListEditPageAdapter.pageController
                }

                _editPageFragment
            }
            else -> { throw IllegalArgumentException() }
        }
    }

    // ページ数
    override fun getCount(): Int {
        return 2
    }

    abstract fun createListPageFragment(): BaseListPageFragment
    abstract fun createEditPageFragment(): BaseEditPageFragment
}



/******************************************************************************************************/
// ページ共通
/******************************************************************************************************/

/**
 * ページフラグメント 基底.
 */
abstract class BasePageFragment : Fragment() {
    lateinit var pageController: BaseListEditPageController
    protected var viewCreatedListener: OnFragmentViewCreatedListener? = null

    protected lateinit var activity: AppCompatActivity

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        activity = context as AppCompatActivity
    }

    fun setOnFragmentViewCreatedListener(listener: OnFragmentViewCreatedListener) {
        viewCreatedListener = listener
    }
}


/******************************************************************************************************/
// 一覧ページ関連
/******************************************************************************************************/

/**
 * 基底 リストページ フラグメント.
 */
abstract class BaseListPageFragment : BasePageFragment() {

    protected val EDIT_PAGE_MODE_NEW = 1
    protected val EDIT_PAGE_MODE_EDIT = 2

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.content_searchable_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initializeOnViewCreated(view)

        viewCreatedListener?.onFragmentViewCreated(this, view, savedInstanceState)
    }

    protected open fun initializeOnViewCreated(view: View) {
        // ＋ボタン ClickListener
        fab_add_list_item.setOnClickListener {
            pageController.let {
                actBeforeSelectingEditPage(EDIT_PAGE_MODE_NEW, null)
                it.selectEditPage()
            }
        }
    }

    protected fun toEditPage(item: Any?) {
        actBeforeSelectingEditPage(EDIT_PAGE_MODE_EDIT, item)
        pageController.selectEditPage()
    }

    abstract fun actBeforeSelectingEditPage(mode: Int, item: Any?)
}


/******************************************************************************************************/
// 編集ページ関連
/******************************************************************************************************/

abstract class BaseEditPageFragment : BasePageFragment()




