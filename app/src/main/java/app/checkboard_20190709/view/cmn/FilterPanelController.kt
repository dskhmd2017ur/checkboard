package app.checkboard_20190709.view.cmn

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import app.checkboard_20190709.R

class FilterPanelController(
    private val filterViewLayout : LinearLayout,
    activity: Activity) {

    private var _filterHeader : RelativeLayout
    private var _arrowImage : ImageView
    private var _filterList : RecyclerView
//    private val _filterHeaderHeight: Int

    private var _filterOpened = false

//    val fileterHeaderHeight: Int
//        get() = _filterHeaderHeight

    var visible: Boolean
        get() = filterViewLayout.visibility == View.VISIBLE
        set(value) {
            if (value) {
                filterViewLayout.visibility =View.VISIBLE
            }
            else {
                filterViewLayout.visibility =View.INVISIBLE
            }
        }

    interface FilterControlEventListener {
        fun beforeFilterAnimation(filterWillOpen: Boolean)
        fun filterHeaderClicked(filterOpened: Boolean)
    }

    private var _eventListener: FilterControlEventListener? = null

    fun setEventListener(l: FilterControlEventListener) {
        _eventListener = l
    }

    init {

        _arrowImage = filterViewLayout.findViewById(R.id.img_filter_arrow)
        _filterHeader = filterViewLayout.findViewById(R.id.pnl_filter_header_view)
        _filterList = filterViewLayout.findViewById(R.id.rv_filter_list)

//        _filterHeaderHeight = _filterHeader.layoutParams.height

        _filterHeader.setOnClickListener{
            _eventListener?.beforeFilterAnimation(!_filterOpened)
            setAnimation()
            _eventListener?.filterHeaderClicked(_filterOpened)
        }

        // 画面サイズに合わせてレイアウトサイズを決定する。
        setLayoutSize(activity)
    }

    fun closeFilter() {
        if (_filterOpened) setAnimation()
    }

//    fun callFilterHeaderOnClick() {
//        _filterHeader.callOnClick()
//    }

    // RecyclerView などのサイズを設定する。
    private fun setLayoutSize(activity: Activity){
        // 画面サイズを取得
        var screenSize = TargetSizeChecker.getDisplaySize(activity)

        // 対象 View のサイズに関する値
        var recyclerViewLayoutHeight = (screenSize.y * 0.75).toInt()
        var listHeaderLayoutHeight = (screenSize.y * 0.1).toInt()
        var recyclerViewHeight = recyclerViewLayoutHeight - listHeaderLayoutHeight
        Log.d("heights", recyclerViewHeight.toString())

        // 対象 View にサイズを挿入。
        var recyclerViewLayoutParams = filterViewLayout.layoutParams
        recyclerViewLayoutParams.height = recyclerViewLayoutHeight
        filterViewLayout.layoutParams = recyclerViewLayoutParams

        var listHeaderLayoutParams =
            _filterHeader.layoutParams as LinearLayout.LayoutParams
        listHeaderLayoutParams.height = listHeaderLayoutHeight
        listHeaderLayoutParams.setMargins(0, recyclerViewHeight, 0,0)
        _filterHeader.layoutParams = listHeaderLayoutParams

        var recyclerViewParams = _filterList.layoutParams
        recyclerViewParams.height = recyclerViewHeight
        _filterList.layoutParams = recyclerViewParams
    }

    /*
    * RecyclerView の開閉アニメーション
    * ２つのアニメーションを同時に実行している。
    * ・矢印アイコンの向きを変える
    * ・RecyclerView のサイズを変更
    * */
    private fun setAnimation(){

        // 複数の Animator を格納するリスト
        val animatorList = ArrayList<Animator>()

        // 矢印アイコンの回転アニメーション
        var fromRotation = 0f
        var toRotation = 180f

        var recyclerViewSize = _filterList.layoutParams.height

        var fromTransitionY = 0f
        var toTransitionY = -1f * recyclerViewSize

        if (_filterOpened) {
            fromRotation = 180f
            toRotation = 360f

            fromTransitionY = -1f * recyclerViewSize
            toTransitionY = 0f

            _filterOpened = false
        }else{
            _filterOpened = true
        }

        val objectRotateAnimatar =
            ObjectAnimator.ofFloat(_arrowImage, "rotation", fromRotation, toRotation)
        objectRotateAnimatar.duration = 500
        animatorList.add(objectRotateAnimatar)

        val objectTransitionYAnimatorForRecyclerView =
            ObjectAnimator.ofFloat(_filterList, "translationY", fromTransitionY, toTransitionY)
        objectTransitionYAnimatorForRecyclerView.duration = 500
        animatorList.add(objectTransitionYAnimatorForRecyclerView)

        val objectTransitionYAnimatorForHeaderLayout =
            ObjectAnimator.ofFloat(_filterHeader, "translationY", fromTransitionY, toTransitionY)
        objectTransitionYAnimatorForHeaderLayout.duration = 500
        animatorList.add(objectTransitionYAnimatorForHeaderLayout)

        val animatorSet = AnimatorSet()
        animatorSet.playTogether(animatorList)
        animatorSet.start()
    }
}