package app.checkboard_20190709.view.cmn

import android.support.v4.view.ViewPager
import android.view.View

// https://qiita.com/hkusu/items/4eb13f641b2b3559388a
class PageSwipeTransformer : ViewPager.PageTransformer {
    private var minAlphaOut = 1.0f
    private var minAlphaIn = 1.0f
    private var minScaleOut = 1.0f
    private var minScaleIn = 1.0f
    private var positionXOffsetIn = 1.0f
    private var positionXOffsetOut = -1.0f
    private var positionYOffsetIn = 0.0f
    private var positionYOffsetOut = 0.0f

    fun setMinAlphaOut(minAlpha: Float): PageSwipeTransformer {
        this.minAlphaOut = minAlpha
        return this
    }

    fun setMinAlphaIn(minAlpha: Float): PageSwipeTransformer {
        this.minAlphaIn = minAlpha
        return this
    }

    fun setMinScaleOut(minScale: Float): PageSwipeTransformer {
        this.minScaleOut = minScale
        return this
    }

    fun setMinScaleIn(minScale: Float): PageSwipeTransformer {
        this.minScaleIn = minScale
        return this
    }

    fun setPositionXOffsetIn(offsetPositionX: Float): PageSwipeTransformer {
        this.positionXOffsetIn = offsetPositionX
        return this
    }

    fun setPositionXOffsetOut(offsetPositionX: Float): PageSwipeTransformer {
        this.positionXOffsetOut = offsetPositionX
        return this
    }

    fun setPositionYOffsetIn(offsetPositionY: Float): PageSwipeTransformer {
        this.positionYOffsetIn = offsetPositionY
        return this
    }

    fun setPositionYOffsetOut(offsetPositionY: Float): PageSwipeTransformer {
        this.positionYOffsetOut = offsetPositionY
        return this
    }



    override fun transformPage(page: View, position: Float) {

        val width = page.width
        val height = page.height

        if (-1.0f < position && position <= 0) {
            page.visibility = View.VISIBLE
            page.alpha = Math.max(minAlphaOut, 1 + position)
            page.scaleX = Math.max(minScaleOut, 1 + position)
            page.scaleY = Math.max(minScaleOut, 1 + position)
            page.translationX = width * -position + width * -position * positionXOffsetOut
            page.translationY = height * -position * positionYOffsetOut
        }

        if (0 < position && position < 1.0f) {
            page.visibility = View.VISIBLE
            page.alpha = Math.max(minAlphaIn, 1 - position)
            page.scaleX = Math.max(minScaleIn, 1 - position)
            page.scaleY = Math.max(minScaleIn, 1 - position)
            page.translationX = -width * position + width * position * positionXOffsetIn
            page.translationY = height * position * positionYOffsetIn
        }

        if (position <= -1.0f || 1.0f <= position) {
            page.visibility = View.GONE
        }
    }
}