package app.checkboard_20190709.view.main

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import app.checkboard_20190709.R
import app.checkboard_20190709.db.ElementGroup


@SuppressLint("SetTextI18n")
class GroupFilterListAdapter (
    var dataset : MutableList<ElementGroup>)
        : RecyclerView.Adapter<GroupFilterListAdapter.ViewHolder>()  {

    interface GroupFilterListEventListener {
        fun onItemClick(position: Int, item: ElementGroup)
    }

    private var _eventListener: GroupFilterListEventListener? = null

    fun setEventListener(l: GroupFilterListEventListener) {
        _eventListener = l
    }

    private var _recyclerView: RecyclerView? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        _recyclerView = recyclerView
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        _recyclerView = null
    }

    open inner class ViewHolder(cell: View) : RecyclerView.ViewHolder(cell) {
        val titleLabel: TextView = cell.findViewById(R.id.lbl_item_title)

        init {
            cell.setOnClickListener {
                Log.d("click", "now")
                _recyclerView?.let {
                    val position = it.getChildAdapterPosition(cell)
                    val item = dataset[position]
                    _eventListener?.onItemClick(position, item)
                }
            }
        }
    }

    //ViewType に応じてレイアウトを決定し、View（ViewHolder）を作成する。
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(
            R.layout.listitem_filter, viewGroup, false)
        return ViewHolder(v)
    }

    //表示するView（ViewHolder）に、データを紐づける。
    override fun onBindViewHolder(holder : ViewHolder, position: Int){
        val rep = dataset[position]
        holder.titleLabel.text = rep.title
    }

    override fun getItemCount(): Int = dataset.size
}

