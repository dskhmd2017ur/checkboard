package app.checkboard_20190709.view.main

import android.annotation.SuppressLint
import android.support.v4.app.FragmentManager
import android.content.DialogInterface
import android.graphics.Canvas
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import app.checkboard_20190709.R
import app.checkboard_20190709.db.CBReport
import app.checkboard_20190709.view.cmn.AlertDialogFragment
import app.checkboard_20190709.view.cmn.DeleteConfirmDialogCreateParam
import app.checkboard_20190709.view.cmn.RecyclerViewSimpleCallBack
import app.checkboard_20190709.view.cmn.SwipeBackgroundHelper


@SuppressLint("SetTextI18n")
class ReportListAdapter (
        var dataset : MutableList<CBReport>,
        private val fragmentManager: FragmentManager)
    : RecyclerView.Adapter<ReportListAdapter.ViewHolder>()  {

    interface ReportListEventListener: ReportListCallBack.OnReportSwipedListener {
        fun onItemClick(position: Int, item: CBReport)
    }

    private var _eventListener: ReportListEventListener? = null

    fun setEventListener(l: ReportListEventListener) {
        _eventListener = l
    }

    private lateinit var _itemTouchHelper : ItemTouchHelper
    private var _recyclerView: RecyclerView? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)

        _recyclerView = recyclerView

        // RecyclerView のドラッグ、スワイプ操作に関する設定
        val callback = ReportListCallBack(recyclerView, dataset, fragmentManager)
        _eventListener?.let { callback.setOnReportSwipedListener(_eventListener) }

        _itemTouchHelper = ItemTouchHelper(callback)
        _itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        _recyclerView = null
    }

    open inner class ViewHolder(cell: View) : RecyclerView.ViewHolder(cell) {
        val titleLabel: TextView = cell.findViewById(R.id.lbl_item_title)
        val metaInfoLabel: TextView = cell.findViewById(R.id.lbl_item_meta_info)
        private val hamburgerImageView : ImageView = cell.findViewById(R.id.hamburger_image_view)

        init {

            hamburgerImageView.visibility = View.INVISIBLE

            cell.setOnClickListener {
                Log.d("click", "now")
                _recyclerView?.let {
                    val position = it.getChildAdapterPosition(cell)
                    val item = dataset[position]
                    _eventListener?.onItemClick(position, item)
                }
            }
        }
    }

    //ViewType に応じてレイアウトを決定し、View（ViewHolder）を作成する。
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(
            R.layout.listitem_simple_01, viewGroup, false)
        return ViewHolder(v)
    }

    //表示するView（ViewHolder）に、データを紐づける。
    override fun onBindViewHolder(holder : ViewHolder, position: Int){

        val rep = dataset[position]

        holder.titleLabel.text = rep.title
        holder.metaInfoLabel.text = rep.metaInfo
    }

    override fun getItemCount(): Int = dataset.size

//    override fun getItemViewType(position: Int): Int {
//        return super.getItemViewType(position)
//    }

    fun dispose() {
        // 初回の一覧表示では問題ないが、ページ切り替わりの2回目以降、
        // バインドが残っている？ためおかしくなる
        _itemTouchHelper.attachToRecyclerView(null)
    }
}

class ReportListCallBack(
    private val recyclerView: RecyclerView,
    private val dataSet: MutableList<CBReport>,
    private val fragmentManager: FragmentManager)
        : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

    override fun onMove(
        recyclerView: RecyclerView?,
        viewHolder: RecyclerView.ViewHolder?,
        target: RecyclerView.ViewHolder?
    ): Boolean {
        return false
    }

    interface OnReportSwipedListener {
        fun onReportSwiped(reportId: Long, position: Int)
    }

    private var  _onReportSwipedListener: OnReportSwipedListener? = null

    fun setOnReportSwipedListener(l: OnReportSwipedListener?) {
        _onReportSwipedListener = l
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

        val dlg = AlertDialogFragment()
        dlg.dialogCreateParam = DeleteConfirmDialogCreateParam().apply {
            this.setOnPositiveClickListener(
                DialogInterface.OnClickListener { dialogInterface, i ->
                    _onReportSwipedListener?.onReportSwiped(
                        dataSet[viewHolder.adapterPosition].id, viewHolder.adapterPosition)
                })
            this.setOnNegativeClickListener(
                DialogInterface.OnClickListener { dialogInterface, i ->
                    recyclerView.adapter.notifyItemChanged(viewHolder.adapterPosition)
                })
        }

        dlg.show(fragmentManager, "dialog")
    }

    override fun onChildDraw(canvas: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
                             dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            val viewItem = viewHolder.itemView
            SwipeBackgroundHelper.paintDrawCommandToStart(
                canvas, viewItem, R.drawable.ic_trash , dX)

            // android.R.drawable.ic_menu_delete ← 粗い
        }
        super.onChildDraw(canvas, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }

    override fun getSwipeThreshold(viewHolder: RecyclerView.ViewHolder?): Float {
        return 0.8f
    }
}
