package app.checkboard_20190709.view.main

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.view.ViewPager
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.view.View
import app.checkboard_20190709.db.CBReport
import app.checkboard_20190709.db.CBReportItem
import app.checkboard_20190709.view.cmn.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_edit_report.*
import kotlinx.android.synthetic.main.content_list.*
import org.jetbrains.anko.toast

class ReportListEditPageController(
    pager: ViewPager,
    fragmentManager: FragmentManager,
    activity: AppCompatActivity)
        : BaseListEditPageController(pager, fragmentManager, activity) {

    //region property

    var selectedReport: CBReport? = null

    val selectedReportId: Long?
        get() = selectedReport?.id ?: null

    private val reportListAdapter: ReportListAdapter?
        get() = activity.rv_list.adapter as ReportListAdapter

    private val reportItemListAdapter: ReportItemListAdapter?
        get() = activity.rv_item_list.adapter as ReportItemListAdapter

    //endregion

    //region 初期化

    init {
        super.setListEditPageEventListener(
            object : ListEditPageEventListener {

                override fun onFragmentViewCreated(
                    fragment: Fragment, view: View, savedInstanceState: Bundle?) {

                    if (fragment is ReportListFragment) {
                        fragment.loadReportList()
                        activity.supportActionBar?.hide()
                    }
                }

                override fun onListPageSelected() {
                    (editPageFragment as ReportEditFragment).disposeReportItemList()
                    (listPageFragment as ReportListFragment).loadReportList()
                    activity.supportActionBar?.hide()
                    activity.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
                }

                override fun onEditPageSelected() {
                    val reportId = selectedReportId
                    (listPageFragment as ReportListFragment).disposeReportList()
                    (editPageFragment as ReportEditFragment).loadReportEditInfo(reportId)
                    activity.supportActionBar?.show()
                    activity.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
                }
            })
    }

    //endregion

    //region override

    /**
     * ページアダプタ 生成.
     */
    override fun createPageAdapter(): BaseListEditPageAdapter {

        val adapter = ReportListEditPageAdapter(this, fragmentManager)
            adapter.setDbRegistEventListener(
                object : ReportListEditPageAdapter.ReportRegistEventListener {

                    override fun onReportRegisted(report: CBReport, isNewData: Boolean) {
                        (this@ReportListEditPageController).selectedReport = report
                        (editPageFragment as ReportEditFragment).loadReportEditInfoHeader(report.id)
                        activity.toast(if (isNewData) "追加しました" else "修正しました")
                    }

                    override fun onReportItemRegisted(
                        reportItem: CBReportItem, position: Int, isNewData: Boolean) {

                        val adapter = reportItemListAdapter!!

                        if (isNewData) {
                            adapter.dataset.add(reportItem)
                            val insertIndex = adapter.itemCount - 1
                            adapter.notifyItemInserted(insertIndex)
                            activity.rv_item_list.smoothScrollToPosition(insertIndex)
                        }
                        else {
                            adapter.dataset[position] = reportItem
                            notifyReportItemChanged(position)
                        }

                        (editPageFragment as ReportEditFragment).reloadReportMetaInfo()

                        activity.toast(if (isNewData) "追加しました" else "修正しました")
                    }

                    override fun onReportDeleted(reportId: Long) {
                        activity.toast("削除しました")
                    }

                    override fun onReportDeletedBySwipe(reportId: Long, position: Int) {

                        val adapter = reportListAdapter!!

                        adapter.dataset.removeAt(position)
                        adapter.notifyItemRemoved(position)

                        activity.toast("削除しました")
                    }

                    override fun onReportItemDeleted(reportItemId: Long, position: Int) {

                        val adapter = reportItemListAdapter!!

                        adapter.dataset.removeAt(position)
                        adapter.notifyItemRemoved(position)

                        activity.toast("削除しました")
                    }
            })

        return adapter
    }

    private fun notifyReportItemChanged(position: Int) {
        activity.rv_item_list.adapter.notifyItemChanged(position)
    }

    //endregion
}