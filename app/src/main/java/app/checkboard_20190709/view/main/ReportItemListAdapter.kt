package app.checkboard_20190709.view.main

import android.annotation.SuppressLint
import android.support.v4.app.FragmentManager

import android.content.DialogInterface
import android.graphics.Canvas

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import app.checkboard_20190709.R

import app.checkboard_20190709.db.CBReportItem
import app.checkboard_20190709.db.RealmManager
import app.checkboard_20190709.view.cmn.*
import kotlin.math.abs
import kotlin.math.min


@SuppressLint("SetTextI18n")
class ReportItemListAdapter(
    var dataset : MutableList<CBReportItem>,
    private val fragmentManager: FragmentManager)
        : RecyclerView.Adapter<ReportItemListAdapter.ViewHolder>()  {

    interface ReportItemListEventListener
        : ReportItemListCallBack.OnReportItemSwipedListener {

        fun onItemClick(position: Int, item: CBReportItem)
        fun onCheckedChange(checkReportItem: CBReportItem)
        fun reportItemDispSeqUpdated()
    }

    private var _eventListener: ReportItemListEventListener? = null

    fun setEventListener(l: ReportItemListEventListener) {
        _eventListener = l
    }

    private lateinit var _itemTouchHelper : ItemTouchHelper
    private var _recyclerView: RecyclerView? = null
    private var _dragStartPos: Int? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)

        _recyclerView = recyclerView

        // RecyclerView のドラッグ、スワイプ操作に関する設定
        val callback = ReportItemListCallBack(recyclerView, dataset, fragmentManager)
        _eventListener?.let { callback.setOnReportItemSwipedListener(_eventListener) }

        callback.setOnDraggingListener(
            object : RecyclerViewSimpleCallBack.OnDraggingListener {
                //override fun onStartDragging(viewHolder: RecyclerView.ViewHolder) { }
                override fun onMoveDragging(
                    from: RecyclerView.ViewHolder, to: RecyclerView.ViewHolder) {
                    if (_dragStartPos == null) {
                        _dragStartPos = from.adapterPosition
                    }
                }
                override fun onEndDragging(viewHolder: RecyclerView.ViewHolder) {
                    endDragging(viewHolder)
                }
            }
        )

        _itemTouchHelper = ItemTouchHelper(callback)
        _itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        _recyclerView = null
    }

    @SuppressLint("ClickableViewAccessibility")
    open inner class ViewHolder(cell: View) : RecyclerView.ViewHolder(cell) {
        val titleLabel: TextView = cell.findViewById(R.id.lbl_item_title)
        val qtyLabel: TextView = cell.findViewById(R.id.lbl_item_qty)
        val checkBox: CheckBox = cell.findViewById(R.id.chk_item_check)
        val memoLabel: TextView = cell.findViewById(R.id.lbl_item_memo)
        private val memoPanel: View = cell.findViewById(R.id.pnl_item_memo)
        private val hamburgerImageView : ImageView = cell.findViewById(R.id.hamburger_image_view)
        private val memoPanelHeight = cell.findViewById<LinearLayout>(R.id.pnl_item_memo).layoutParams.height

        init {

            cell.setOnLongClickListener {
                Log.d("longClick", "now")
                it.isPressed = false
                true
            }

            cell.setOnClickListener {
                //filterPanel.closeFilter()
                Log.d("click", "now")

                _recyclerView?.let {
                    val position = it.getChildAdapterPosition(cell)
                    val item = dataset[position]
                    _eventListener?.onItemClick(position, item)
                }
            }

            hamburgerImageView.setOnTouchListener { view, event ->
                if (event.actionMasked == MotionEvent.ACTION_DOWN) {
                    startDragging(this)
                }else if(event.actionMasked == MotionEvent.ACTION_UP){
                    endDragging(this)
                }
                true
            }
        }

        var memoPanelVisibility: Int
            get() = memoPanel.visibility
            set(value) {
                val marginLayoutParams = memoPanel.layoutParams
                when (value) {
                    View.VISIBLE -> marginLayoutParams.height = memoPanelHeight
                    View.INVISIBLE -> marginLayoutParams.height = 0
                }
                memoPanel.layoutParams = marginLayoutParams
                memoPanel.visibility = value
            }
    }

    //ViewType に応じてレイアウトを決定し、View（ViewHolder）を作成する。
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(
            R.layout.listitem_report_item, viewGroup, false)
        return ViewHolder(v)
    }

    //表示するView（ViewHolder）に、データを紐づける。
    override fun onBindViewHolder(holder : ViewHolder, position: Int){

        val repItem = dataset[position]

        holder.checkBox.isChecked = repItem.checked
        holder.titleLabel.text = repItem.title
        holder.qtyLabel.text = repItem.quantityText

        if (repItem.hasMemo) {
            holder.memoLabel.text = repItem.memo
            holder.memoPanelVisibility = View.VISIBLE
        }
        else
        {
            holder.memoLabel.text = ""
            holder.memoPanelVisibility = View.INVISIBLE
        }

        holder.checkBox.setOnClickListener {
            val chk = it as CheckBox
            val updItem = RealmManager
                .updateReportItemCheckedState(repItem.item_id, chk.isChecked)
            dataset[position] = updItem

            _eventListener?.onCheckedChange(updItem)
        }
    }

    override fun getItemCount(): Int = dataset.size

//    override fun getItemViewType(position: Int): Int {
//        return super.getItemViewType(position)
//    }

    // リスト画像をタップすると即座にドラッグ開始
    private fun startDragging(viewHolder : RecyclerView.ViewHolder){
        _itemTouchHelper.startDrag(viewHolder)
        viewHolder.itemView.isPressed = true
    }

    // imageView から指を離した時
    private fun endDragging(viewHolder : RecyclerView.ViewHolder){
        Log.d("endDragging", "now")

        _dragStartPos?.let {
            updateReportItemDispSeq(_dragStartPos!!, viewHolder.adapterPosition)
            _dragStartPos = null
        }

        viewHolder.itemView.isPressed = false
    }

    private fun updateReportItemDispSeq(startPos: Int, currentPos: Int): Boolean {

        if (startPos == currentPos) return false

        val idx = min(startPos, currentPos)
        val cnt = abs(startPos - currentPos)

        val items = mutableListOf<CBReportItem>()
        for (i in idx..idx + cnt) {
            items.add(dataset[i])
        }

        var currentDispSeq = idx

        items.forEach {
            val updItem = RealmManager.
                updateReportItemDispSeq(it.item_id, currentDispSeq)
            dataset[currentDispSeq] = updItem
            currentDispSeq++
        }

        _eventListener?.reportItemDispSeqUpdated()

        return true
    }

    fun dispose() {
        // 初回の一覧表示では問題ないが、ページ切り替わりの2回目以降、
        // バインドが残っている？ためおかしくなる
        _itemTouchHelper.attachToRecyclerView(null)
    }
}

class ReportItemListCallBack(
        recyclerView: RecyclerView,
        dataSet: MutableList<CBReportItem>,
        private val fragmentManager: FragmentManager)
    : RecyclerViewSimpleCallBack<CBReportItem>(recyclerView, dataSet) {

    interface OnReportItemSwipedListener {
        fun onReportItemSwiped(reportItemId: Long, position: Int)
    }

    private var  _onReportItemSwipedListener: OnReportItemSwipedListener? = null

    fun setOnReportItemSwipedListener(l: OnReportItemSwipedListener?) {
        _onReportItemSwipedListener = l
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

        val dlg = AlertDialogFragment()
        dlg.dialogCreateParam = DeleteConfirmDialogCreateParam().apply {
            this.setOnPositiveClickListener(
                DialogInterface.OnClickListener { dialogInterface, i ->
                    _onReportItemSwipedListener?.onReportItemSwiped(
                        dataSet[viewHolder.adapterPosition].item_id, viewHolder.adapterPosition)
                })
            this.setOnNegativeClickListener(
                DialogInterface.OnClickListener { dialogInterface, i ->
                    recyclerView.adapter.notifyItemChanged(viewHolder.adapterPosition)
                })
        }

        dlg.show(fragmentManager, "dialog")
    }

    override fun onChildDraw(canvas: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
                             dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            val viewItem = viewHolder.itemView
            SwipeBackgroundHelper.paintDrawCommandToStart(
                canvas, viewItem, R.drawable.ic_trash , dX)

            // android.R.drawable.ic_menu_delete ← 粗い
        }
        super.onChildDraw(canvas, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }
}
