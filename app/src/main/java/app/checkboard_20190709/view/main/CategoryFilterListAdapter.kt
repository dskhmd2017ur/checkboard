package app.checkboard_20190709.view.main

import android.annotation.SuppressLint
import android.service.autofill.Dataset
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import app.checkboard_20190709.R
import app.checkboard_20190709.view.cmn.FilterPanelController
import app.checkboard_20190709.view.cmn.RecyclerViewSimpleCallBack
import kotlinx.android.synthetic.main.content_searchable_list_with_filter.*
import java.io.File


/*

* RecyclerView に渡す Adapter
*
* ・Adapter ：View とデータセットを紐づける役割を担うクラス。
*
* ・ViewHolder : View 一行ごとの参照を保持する
*
* */

class CategoryFilterListAdapter (
    var dataset : MutableList<CategoryFilterListData>,
    val filterPanel: FilterPanelController)
        : RecyclerView.Adapter<CategoryFilterListAdapter.SampleViewHolder>(){

    private lateinit var _itemTouchHelper : ItemTouchHelper

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)

        // RecyclerView のドラッグ、スワイプ操作に関する設定
        val callback = CategoryFilterListCallBack(recyclerView, dataset)

        callback.setOnDraggingListener(
            object : RecyclerViewSimpleCallBack.OnDraggingListener {
                override fun onMoveDragging(from: RecyclerView.ViewHolder, to: RecyclerView.ViewHolder) {
                    
                }

                //override fun onStartDragging(viewHolder: RecyclerView.ViewHolder) { }
                override fun onEndDragging(viewHolder: RecyclerView.ViewHolder) {
                    endDragging(viewHolder)
                }
            }
        )

        _itemTouchHelper = ItemTouchHelper(callback)
        _itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    /*
     * 行と紐づくViewHolder.
     * viewType ごとに 3 クラスに分かれている。
     * */
    open inner class SampleViewHolder(v : View) : RecyclerView.ViewHolder(v) {
        var titleTextView : TextView = v.findViewById(R.id.title_text_view)
    }

    @SuppressLint("ClickableViewAccessibility")
    open inner class DefaultSampleViewHolder(v : View) : SampleViewHolder(v) {

        var thumbnailImageView : ImageView = v.findViewById(R.id.thumbnail_image_view)
        var hamburgerImageView : ImageView = v.findViewById(R.id.hamburger_image_view)

        init {

            v.setOnLongClickListener{
                Log.d("longClick", "now")
                v.isPressed = false
                true
            }
            v.setOnClickListener {
                filterPanel.closeFilter()
                Log.d("click", "now")
            }

            hamburgerImageView.setOnTouchListener { view, event ->
                if (event.actionMasked == MotionEvent.ACTION_DOWN) {
                    startDragging(this)
                }else if(event.actionMasked == MotionEvent.ACTION_UP){
                    endDragging(this)
                }
                true
            }
        }
    }

    inner class SubSampleViewHolder(v : View)
        : DefaultSampleViewHolder(v){
        var subTitleTextView : TextView = v.findViewById(R.id.subtitle_text_view)
    }

    //ViewType に応じてレイアウトを決定し、View（ViewHolder）を作成する。
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): SampleViewHolder {

        //viewType に応じてレイアウトを変更している。
        return when (viewType){

            CategoryFilterViewType.SECTION.ordinal -> {
                val v = LayoutInflater.from(viewGroup.context)
                    .inflate(R.layout.sample_recyclerview_row_view_section, viewGroup, false)

                SampleViewHolder(v)
            }

            CategoryFilterViewType.DEFAULT.ordinal -> {

                val v = LayoutInflater.from(viewGroup.context)
                    .inflate(R.layout.sample_recyclerview_row_view_default, viewGroup, false)

                DefaultSampleViewHolder(v)
            }
            else -> {

                val v = LayoutInflater.from(viewGroup.context)
                    .inflate(R.layout.sample_recyclerview_row_view_sub, viewGroup, false)

                SubSampleViewHolder(v)
            }
        }
    }

    //表示するView（ViewHolder）に、データを紐づける。
    override fun onBindViewHolder(holder : SampleViewHolder, position: Int){

        holder.titleTextView.text = dataset[position].title

        if (holder is DefaultSampleViewHolder)
            holder.thumbnailImageView.setImageResource(dataset[position].imageResource)
        if (holder is SubSampleViewHolder)
            holder.subTitleTextView.text = dataset[position].subTitle

    }

    override fun getItemCount(): Int = dataset.size

    override fun getItemViewType(position: Int): Int {

        return when(dataset[position].viewType){
            CategoryFilterViewType.SECTION -> CategoryFilterViewType.SECTION.ordinal
            CategoryFilterViewType.SUB -> CategoryFilterViewType.SUB.ordinal
            else -> CategoryFilterViewType.DEFAULT.ordinal
        }
    }

    // リスト画像をタップすると即座にドラッグ開始
    private fun startDragging(viewHolder : RecyclerView.ViewHolder){
        _itemTouchHelper.startDrag(viewHolder)
        viewHolder.itemView.isPressed = true
    }

    // imageView から指を離した時
    private fun endDragging(viewHolder : RecyclerView.ViewHolder){
        Log.d("endDragging", "now")
        viewHolder.itemView.isPressed = false
    }
}

class CategoryFilterListCallBack(
    recyclerView: RecyclerView, 
    dataSet: MutableList<CategoryFilterListData>)
        : RecyclerViewSimpleCallBack<CategoryFilterListData>(recyclerView, dataSet) {

    /*
   * 一部リストアイテム（セクション）はドラッグ・スワイプさせたくないため、以下で制御。
   * https://kotaeta.com/61339696
   * */
    override fun getMovementFlags(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder): Int {

        if (viewHolder.itemViewType == CategoryFilterViewType.SECTION.ordinal) return 0
        return super.getMovementFlags(recyclerView, viewHolder)
    }
}

enum class CategoryFilterViewType{
    DEFAULT, SUB, SECTION
}

data class CategoryFilterListData(var title : String = "",
                                  var subTitle : String = "",
                                  var imageResource : Int = 0,
                                  var viewType : CategoryFilterViewType = CategoryFilterViewType.DEFAULT
)