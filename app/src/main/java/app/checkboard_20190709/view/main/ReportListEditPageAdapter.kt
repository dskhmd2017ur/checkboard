package app.checkboard_20190709.view.main

import android.content.DialogInterface
import android.os.Bundle

import android.support.v4.app.FragmentManager
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import app.checkboard_20190709.R
import app.checkboard_20190709.db.CBReport
import app.checkboard_20190709.db.CBReportItem
import app.checkboard_20190709.db.RealmManager
import app.checkboard_20190709.dto.ReportItemInputData
import app.checkboard_20190709.dto.TitleAndGroupInputData
import app.checkboard_20190709.view.cmn.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_edit_report.*
import kotlinx.android.synthetic.main.content_list.*
import kotlinx.android.synthetic.main.content_searchable_list.*
import kotlinx.android.synthetic.main.content_searchable_list_with_filter.*
import android.support.design.widget.CoordinatorLayout
import app.checkboard_20190709.db.ElementGroup


/**
 * レポートリスト・編集ページアダプタ.
 */
class ReportListEditPageAdapter(
    pageController: BaseListEditPageController,
    fm : FragmentManager
) : BaseListEditPageAdapter(pageController, fm)  {

    interface ReportRegistEventListener :
        ReportListFragment.ReportRegistEventListener,
        ReportEditFragment.ReportRegistEventListener

    private var _dbRegistEventListener: ReportRegistEventListener? = null

    fun setDbRegistEventListener(l: ReportRegistEventListener?) {
        _dbRegistEventListener = l
    }

    override fun createListPageFragment(): BaseListPageFragment {
        return ReportListFragment().apply {
            this.setDbRegistEventListener(_dbRegistEventListener)
        }
    }

    override fun createEditPageFragment(): BaseEditPageFragment {
        return ReportEditFragment().apply {
            this.setDbRegistEventListener(_dbRegistEventListener)
        }
    }
}

/**
 * レポートリスト フラグメント
 */
class ReportListFragment : BaseListPageFragment() {

    interface ReportRegistEventListener {
        fun onReportDeletedBySwipe(reportId: Long, position: Int)
    }

    private var _dbRegistEventListener: ReportRegistEventListener? = null

    fun setDbRegistEventListener(listener: ReportRegistEventListener?) {
        _dbRegistEventListener = listener
    }

    private lateinit var _filterController: FilterPanelController

    private val reportPageController: ReportListEditPageController
        get() = pageController as ReportListEditPageController

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        return inflater.inflate(
            R.layout.content_searchable_list_with_filter,
            container, false)
    }

    override fun initializeOnViewCreated(view: View) {

        super.initializeOnViewCreated(view)

        activity.img_show_nav_menu.setOnClickListener {
            activity.drawerLayout.openDrawer(Gravity.START)
        }

        activity.rv_list.apply{
            setHasFixedSize(true)
            layoutManager = LinearLayoutManagerWithSmoothScroller(activity)
            val divider = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
            this.addItemDecoration(divider)
        }

        initializeFrameViews()
    }

    override fun actBeforeSelectingEditPage(mode: Int, item: Any?) {

        _filterController.closeFilter()

        when (mode) {
            EDIT_PAGE_MODE_EDIT -> {
                reportPageController.selectedReport = item as CBReport
            }
            EDIT_PAGE_MODE_NEW -> {
                pageController.let {
                    reportPageController.selectedReport = null
                }
            }
        }
    }

    private fun initializeFrameViews() {

        // RecyclerView を表示するレイアウトを画面下から出現させるアニメーション設定。
        // フィルタコントローラ
        _filterController = FilterPanelController(
            pnl_filter_view_layout, activity)

        // フィルタヘッダ クリックイベント.
        _filterController.setEventListener(
            object : FilterPanelController.FilterControlEventListener {
                override fun beforeFilterAnimation(filterWillOpen: Boolean) {
                    if (filterWillOpen) loadGroupFilterList()
                }
                override fun filterHeaderClicked(filterOpened: Boolean) {
                    if (filterOpened) fab_add_list_item.hide() else fab_add_list_item.show()
                }
            })

        rv_filter_list.apply{
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(activity)
            val divider = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
            this.addItemDecoration(divider)
        }

        val lp = activity.fab_add_list_item.
            layoutParams as CoordinatorLayout.LayoutParams
        lp.bottomMargin = resources.getDimension(R.dimen.fab_bottomMargin_wide).toInt()
        activity.fab_add_list_item.layoutParams = lp

        search_view.setOnQueryTextFocusChangeListener { v, hasFocus ->
            if(hasFocus) { //受け取った時
                fab_add_list_item.hide()
            } else {
                //離れた時
            }
        }

        // ソフトキーボードのVisibilityイベント.
        setKeyboardVisibilityListener(search_view_layout,
            object : OnKeyboardVisibilityListener {
                override fun onVisibilityChanged(visible: Boolean) {
                    Log.i(
                        "Keyboard state",
                        if (visible) "Keyboard is active" else "Keyboard is Inactive")

                    if(visible){  //受け取った時
                        _filterController.closeFilter()
                        _filterController.visible = false
                        fab_add_list_item.hide()
                    } else { //離れた時
                        _filterController.visible = true
                        fab_add_list_item.show()
                    }
                }
            })

        search_view.setIconifiedByDefault(false)
    }

    /**
     * フィルターグループ　ロード.
     */
    fun loadGroupFilterList() {

        val groups = RealmManager.selectElementGroups()
        val adapter = GroupFilterListAdapter(groups.toMutableList())

        adapter.setEventListener(
            object : GroupFilterListAdapter.GroupFilterListEventListener {
                override fun onItemClick(position: Int, item: ElementGroup) {
                    _filterController.closeFilter()
                }
            }
        )

        rv_filter_list.apply{
            this.adapter = adapter
        }
    }

    /**
     * レポートリスト ロード.
     */
    fun loadReportList() {

        disposeReportList()

        val reports = RealmManager.selectReports()
        val adapter = ReportListAdapter(
            reports.toMutableList(),
            activity.supportFragmentManager)

        adapter.setEventListener(
            object : ReportListAdapter.ReportListEventListener {
                override fun onReportSwiped(reportId: Long, position: Int) {
                    deleteReport(reportId, position, _dbRegistEventListener)
                }

                override fun onItemClick(position: Int, item: CBReport) {
                    toEditPage(item)
                }
            }
        )

        activity.rv_list.apply{
            this.adapter = adapter
        }
    }

    /**
     * DB削除　レポート
     */
    private fun deleteReport(
        reportId: Long,
        position: Int,
        registListener: ReportRegistEventListener?) {

        RealmManager.deleteReport(reportId)
        registListener?.onReportDeletedBySwipe(reportId, position)
    }

    fun disposeReportList() {
        activity.rv_list?.adapter?.let {
            (it as ReportListAdapter).dispose()
        }
    }
}

/**
 * レポート編集 フラグメント
 */
class ReportEditFragment : BaseEditPageFragment() {

    //region property

    private val reportPageController: ReportListEditPageController
        get() = pageController as ReportListEditPageController

    //endregion

    //region ReportRegistEventListener

    interface ReportRegistEventListener {

        fun onReportRegisted(report: CBReport, isNewData: Boolean)
        fun onReportDeleted(reportId: Long)

        fun onReportItemRegisted(reportItem: CBReportItem, position: Int, isNewData: Boolean)
        fun onReportItemDeleted(reportItemId: Long, position: Int)
    }

    private var _dbRegistEventListener: ReportRegistEventListener? = null

    fun setDbRegistEventListener(listener: ReportRegistEventListener?) {
        _dbRegistEventListener = listener
    }

    //endregion

    //region override

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        return inflater.inflate(
            R.layout.content_edit_report, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // タイトル部 ClickListener.
        lbl_edit_title.setOnClickListener {

//            this.activity.toast("test")
//            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show()

            // タイトル・グループ編集ダイアログ.
            val dialog = TitleAndGroupEditDialog()

            dialog.setOnClickLister(
                object: TitleAndGroupEditDialog.OnClickListener {
                    override fun onPositiveClick(inputData: TitleAndGroupInputData) {
                        registReport(inputData, _dbRegistEventListener)
                    }
                    override fun onNegativeClick() {}
                }
            )

            dialog.arguments = Bundle().apply {
                this@ReportEditFragment.reportPageController.selectedReport?.let {
                    this.putCharSequence("arg_title", it.title )
                    if (it.group_id != null) {
                        this.putLong("arg_group_id", it.group_id!!)
                    }
                }
            }
            dialog.show(pageController.fragmentManager, "dialog")
        }

        // 画面上部 削除ボタン ClickListener
        activity.img_del.setOnClickListener {
            AlertDialogFragment().apply {
                this.dialogCreateParam = DeleteConfirmDialogCreateParam().apply {
                    this.setOnPositiveClickListener(
                        DialogInterface.OnClickListener { dialogInterface, i ->
                            val reportId = reportPageController.selectedReportId!!
                            deleteReport(reportId, _dbRegistEventListener)
                            pageController.selectListPage()
                        })
                }
            }.show(fragmentManager, "dialog")
        }

        // 項目追加 プラス画像 ClickListener
        img_add_item.setOnClickListener {
            showReportItemEditDialog(null, -1)
        }

        // 項目追加 ラベル ClickListener
        lbl_add_item.setOnClickListener {
            showReportItemEditDialog(null, -1)
        }

        viewCreatedListener?.onFragmentViewCreated(
            this, view, savedInstanceState)
    }

    //endregion

    /**
     * レポート項目 編集ダイアログ表示.
     */
    fun showReportItemEditDialog(
        reportItem: CBReportItem?, position: Int) {

        val dialog = ReportItemEditDialog()

        dialog.setOnClickLister(
            object: ReportItemEditDialog.OnClickListener {
                override fun onPositiveClick(inputData: ReportItemInputData) {
                    registReportItem(
                        reportPageController.selectedReportId!!,
                        reportItem?.item_id,
                        inputData,
                        position, _dbRegistEventListener)
                }

                override fun onDeleteClick(reportItemId: Long?) {
                    if (reportItemId == null) return
                    AlertDialogFragment().apply {
                        this.dialogCreateParam = DeleteConfirmDialogCreateParam().apply {
                            this.setOnPositiveClickListener(
                                DialogInterface.OnClickListener { dialogInterface, i ->
                                    deleteReportItem(reportItemId, position, _dbRegistEventListener)
                                })
                        }
                    }.show(fragmentManager, "dialog")
                }

                override fun onNegativeClick() {}
            }
        )

        val args = Bundle()
        args.putLong("arg_item_id", reportItem?.item_id ?: 0L)
        args.putCharSequence("arg_item_title", reportItem?.title )
        args.putFloat("arg_item_qty", reportItem?.quantity ?: 0F)
        args.putFloat("arg_item_actual_qty", reportItem?.actual_quantity ?: 0F)
        args.putCharSequence("arg_item_memo", reportItem?.memo)
        dialog.arguments = args

        dialog.show(pageController.fragmentManager, "dialog")
    }

    /**
     * DB削除　レポート
     */
    private fun deleteReport(
        reportId: Long,
        registListener: ReportRegistEventListener?) {

        RealmManager.deleteReport(reportId)
        registListener?.onReportDeleted(reportId)
    }

    /**
     * DB登録　レポート
     */
    private fun registReport(
        inputData: TitleAndGroupInputData,
        registListener: ReportRegistEventListener?) {

        // ①グループの登録
        var groupId: Long? = null

        inputData.groupItem?.let {
            groupId = when {
                it.isNone -> null
                it.isNew -> {
                    when (val newGrpTitle = inputData.newGroupTitle.trim()) {
                        "" -> null
                        else -> {
                            val newGrp = RealmManager
                                .registElementGroup(null, newGrpTitle)
                            newGrp.id
                        }
                    }
                }
                else -> it.code.toLong()
            }
        }

        // ②レポートの登録
        val reportId = reportPageController.selectedReportId
        val reportTitle =  inputData.title.trim()
        if (reportTitle == "") return

        val registedReport = RealmManager.registReport(
            reportId, reportTitle, groupId)

        registListener?.onReportRegisted(
            registedReport, reportId == null)
    }

    /**
     * DB登録 レポート項目
     */
    private fun registReportItem(
        reportId: Long, itemId: Long?,
        inputData: ReportItemInputData,
        position: Int,
        registListener: ReportRegistEventListener?) {

        val registedItem =  RealmManager.registReportItem(
            reportId, itemId, inputData)

        registListener?.onReportItemRegisted(
            registedItem, position,itemId == null)
    }

    fun deleteReportItem(itemId: Long, position: Int) {
        deleteReportItem(itemId, position, _dbRegistEventListener)
    }

    /**
     * DB削除　レポート項目
     */
    private fun deleteReportItem(
        itemId: Long,
        position: Int,
        registListener: ReportRegistEventListener?) {

        RealmManager.deleteReportItem(itemId)
        registListener?.onReportItemDeleted(itemId, position)
    }


    //region 各メソッド

    /**
     * レポート編集情報 ロード.
     */
    fun loadReportEditInfo(reportId: Long?) {

        loadReportEditInfoHeader(reportId)

        // レポート項目リストのロード.
        loadReportItemList(reportId)
    }

    /**
     * レポート編集情報 ヘッダ ロード.
     */
    fun loadReportEditInfoHeader(reportId: Long?) {

        activity.lbl_edit_title.text = ""
        activity.lbl_edit_group.text = ""
        activity.lbl_meta_info.text = ""

        activity.img_del.visibility = View.INVISIBLE
        activity.pnl_add_item.visibility = View.INVISIBLE

        if (reportId != null) {
            activity.img_del.visibility = View.VISIBLE
            activity.pnl_add_item.visibility = View.VISIBLE

            val report = RealmManager.selectReport(reportId)

            report?.let {
                // レポートタイトルの設定.
                activity.lbl_edit_title.text = it.title
                activity.lbl_meta_info.text = it.metaInfo
                activity.lbl_edit_group.text = "グループ：" + getGroupTitle(it.group_id)
            }
        }
    }

    /**
     * グループタイトル取得.
     */
    private fun getGroupTitle(groupId: Long?): String {
        var title = "未分類"
        groupId?.let {
            RealmManager.selectElementGroup(it)?.let { eleGrp ->
                title = eleGrp.title
            }
        }
        return title
    }

    /**
     * レポート メタ情報リロード.
     */
    fun reloadReportMetaInfo() {
        val report = RealmManager.selectReport(
            reportPageController.selectedReportId!!)
        report?.let {
            activity.lbl_meta_info.text = it.metaInfo
        }
    }

    /**
     * レポート項目リスト ロード.
     */
    private fun loadReportItemList(
        reportId: Long?, selectionPos: Int = 0) {

        disposeReportItemList()
        activity.rv_item_list.adapter = null

        val reportItems = when (reportId) {
            null -> mutableListOf<CBReportItem>()
            else -> RealmManager.selectReportItems(reportId).toMutableList()
        }

        val adapter = ReportItemListAdapter(
            reportItems, activity.supportFragmentManager)

        adapter.setEventListener(
            object : ReportItemListAdapter.ReportItemListEventListener {
                override fun reportItemDispSeqUpdated() {
                    reloadReportMetaInfo()
                }

                override fun onCheckedChange(checkReportItem: CBReportItem) {
                    reloadReportMetaInfo()
                }

                override fun onReportItemSwiped(reportItemId: Long, position: Int) {
                    deleteReportItem(reportItemId, position)
                }

                override fun onItemClick(position: Int, item: CBReportItem) {
                    showReportItemEditDialog(item, position)
                }
            }
        )

        activity.rv_item_list.apply{
            setHasFixedSize(true)
            layoutManager = LinearLayoutManagerWithSmoothScroller(activity)
            this.adapter = adapter
            val divider = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
            this.addItemDecoration(divider)
        }

        activity.rv_item_list.layoutManager.scrollToPosition(
            if (selectionPos <= (adapter.itemCount - 1)) selectionPos else 0
        )
    }

    fun disposeReportItemList() {
        activity.rv_item_list?.adapter?.let {
            (it as ReportItemListAdapter).dispose()
        }
    }

    //endregion
}

