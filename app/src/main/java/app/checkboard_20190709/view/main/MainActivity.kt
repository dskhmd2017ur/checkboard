package app.checkboard_20190709.view.main


import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import app.checkboard_20190709.R
import app.checkboard_20190709.view.group_mng.GroupMngActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_pager_container.*
import org.jetbrains.anko.startActivity


interface OnKeyboardVisibilityListener {
    fun onVisibilityChanged(visible: Boolean)
}

class MainActivity : AppCompatActivity() {

    private lateinit var _pageController: ReportListEditPageController


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // アクションバー(ツールバー).
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.setHomeButtonEnabled(true)
        }
        supportActionBar?.hide()

        // ページ制御.
        _pageController = ReportListEditPageController(
            pager as ViewPager, supportFragmentManager, this)
        _pageController.initialize()

        navigation_view?.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_item_edit_group -> {
                    startActivity<GroupMngActivity> ()
                    true
                }
                R.id.nav_item_edit_template -> {
                    true
                }
                R.id.nav_item_exit -> {
                    finish()
                    true
                }
                else ->  false
            }
        }
    }

    override fun onBackPressed() {

        when {
            _pageController.isListPageSelected -> finish()
            _pageController.isEditPageSelected -> _pageController.selectListPage()
            else -> super.onBackPressed()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onResume() {
        super.onResume()
//        search_view.setQuery("", false)
//        search_view_layout.requestFocus()
    }

    //　テスト用
    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
//        Log.d("recyclerViewSize", TargetSizeChecker.getViewSize(recyclerViewLayout).toString())
//        Log.d("recyclerViewLayoutSize", TargetSizeChecker.getViewSize(recyclerViewLayout).toString())
//        Log.d("listHeaderLayoutSize", TargetSizeChecker.getViewSize(listHeaderLayout).toString())
    }
}




