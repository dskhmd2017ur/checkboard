package app.checkboard_20190709.view.group_mng

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.graphics.Canvas
import android.support.v4.app.FragmentManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import app.checkboard_20190709.R
import app.checkboard_20190709.db.ElementGroup
import app.checkboard_20190709.db.RealmManager
import app.checkboard_20190709.view.cmn.AlertDialogFragment
import app.checkboard_20190709.view.cmn.DeleteConfirmDialogCreateParam
import app.checkboard_20190709.view.cmn.RecyclerViewSimpleCallBack
import app.checkboard_20190709.view.cmn.SwipeBackgroundHelper
import kotlin.math.abs
import kotlin.math.min


@SuppressLint("SetTextI18n")
class GroupListAdapter(
    var dataset : MutableList<ElementGroup>,
    private val fragmentManager: FragmentManager)
        : RecyclerView.Adapter<GroupListAdapter.ViewHolder>()  {

    interface GroupListEventListener
        : GroupListCallBack.OnGroupSwipedListener {

        fun onItemClick(position: Int, item: ElementGroup)
        fun dispSeqUpdated()
    }

    private var _eventListener: GroupListEventListener? = null

    fun setEventListener(l: GroupListEventListener) {
        _eventListener = l
    }

    private lateinit var _itemTouchHelper : ItemTouchHelper
    private var _recyclerView: RecyclerView? = null
    private var _dragStartPos: Int? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)

        _recyclerView = recyclerView

        // RecyclerView のドラッグ、スワイプ操作に関する設定
        val callback = GroupListCallBack(recyclerView, dataset, fragmentManager)
        _eventListener?.let { callback.setOnGroupSwipedListener(_eventListener) }

        callback.setOnDraggingListener(
            object : RecyclerViewSimpleCallBack.OnDraggingListener {
                override fun onMoveDragging(
                    from: RecyclerView.ViewHolder, to: RecyclerView.ViewHolder) {
                    if (_dragStartPos == null) {
                        _dragStartPos = from.adapterPosition
                    }
                }
                override fun onEndDragging(viewHolder: RecyclerView.ViewHolder) {
                    endDragging(viewHolder)
                }
            }
        )

        _itemTouchHelper = ItemTouchHelper(callback)
        _itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        _recyclerView = null
    }

    @SuppressLint("ClickableViewAccessibility")
    open inner class ViewHolder(cell: View) : RecyclerView.ViewHolder(cell) {
        val titleLabel: TextView = cell.findViewById(R.id.lbl_item_title)
        private val metaInfoLabel: TextView = cell.findViewById(R.id.lbl_item_meta_info)
        private val hamburgerImageView : ImageView = cell.findViewById(R.id.hamburger_image_view)

        init {

            metaInfoLabel.visibility = View.INVISIBLE
            metaInfoLabel.height = 0

            cell.setOnLongClickListener {
                Log.d("longClick", "now")
                it.isPressed = false
                true
            }

            cell.setOnClickListener {
                //filterPanel.closeFilter()
                Log.d("click", "now")

                _recyclerView?.let {
                    val position = it.getChildAdapterPosition(cell)
                    val item = dataset[position]
                    _eventListener?.onItemClick(position, item)
                }
            }

            hamburgerImageView.setOnTouchListener { view, event ->
                if (event.actionMasked == MotionEvent.ACTION_DOWN) {
                    startDragging(this)
                }else if(event.actionMasked == MotionEvent.ACTION_UP){
                    endDragging(this)
                }
                true
            }
        }
    }

    //ViewType に応じてレイアウトを決定し、View（ViewHolder）を作成する。
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(
            R.layout.listitem_simple_01, viewGroup, false)
        return ViewHolder(v)
    }

    //表示するView（ViewHolder）に、データを紐づける。
    override fun onBindViewHolder(holder : ViewHolder, position: Int){
        val grp = dataset[position]
        holder.titleLabel.text = grp.title
    }

    override fun getItemCount(): Int = dataset.size

    // リスト画像をタップすると即座にドラッグ開始
    private fun startDragging(viewHolder : RecyclerView.ViewHolder){
        _itemTouchHelper.startDrag(viewHolder)
        viewHolder.itemView.isPressed = true
    }

    // imageView から指を離した時
    private fun endDragging(viewHolder : RecyclerView.ViewHolder){
        Log.d("endDragging", "now")

        _dragStartPos?.let {
            updateGroupDispSeq(_dragStartPos!!, viewHolder.adapterPosition)
            _dragStartPos = null
        }

        viewHolder.itemView.isPressed = false
    }

    private fun updateGroupDispSeq(startPos: Int, currentPos: Int): Boolean {

        if (startPos == currentPos) return false

        val idx = min(startPos, currentPos)
        val cnt = abs(startPos - currentPos)

        val items = mutableListOf<ElementGroup>()
        for (i in idx..idx + cnt) {
            items.add(dataset[i])
        }

        var currentDispSeq = idx

        items.forEach {
            val updItem = RealmManager.
                updateElementGroupDispSeq(it.id, currentDispSeq)
            dataset[currentDispSeq] = updItem
            currentDispSeq++
        }

        _eventListener?.dispSeqUpdated()

        return true
    }

    fun dispose() {
        // 初回の一覧表示では問題ないが、ページ切り替わりの2回目以降、
        // バインドが残っている？ためおかしくなる
        _itemTouchHelper.attachToRecyclerView(null)
    }
}

class GroupListCallBack(
        recyclerView: RecyclerView,
        dataSet: MutableList<ElementGroup>,
        private val fragmentManager: FragmentManager)
    : RecyclerViewSimpleCallBack<ElementGroup>(recyclerView, dataSet) {

    interface OnGroupSwipedListener {
        fun onGroupSwiped(groupId: Long, position: Int)
    }

    private var _onGroupSwipedListener: OnGroupSwipedListener? = null

    fun setOnGroupSwipedListener(l: OnGroupSwipedListener?) {
        _onGroupSwipedListener = l
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

        val dlg = AlertDialogFragment()
        dlg.dialogCreateParam = DeleteConfirmDialogCreateParam().apply {
            this.setOnPositiveClickListener(
                DialogInterface.OnClickListener { dialogInterface, i ->
                    _onGroupSwipedListener?.onGroupSwiped(
                        dataSet[viewHolder.adapterPosition].id, viewHolder.adapterPosition)
                })
            this.setOnNegativeClickListener(
                DialogInterface.OnClickListener { dialogInterface, i ->
                    recyclerView.adapter.notifyItemChanged(viewHolder.adapterPosition)
                })
        }

        dlg.show(fragmentManager, "dialog")
    }

    override fun onChildDraw(canvas: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
                             dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            val viewItem = viewHolder.itemView
            SwipeBackgroundHelper.paintDrawCommandToStart(
                canvas, viewItem, R.drawable.ic_trash , dX)

            // android.R.drawable.ic_menu_delete ← 粗い
        }
        super.onChildDraw(canvas, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }
}
