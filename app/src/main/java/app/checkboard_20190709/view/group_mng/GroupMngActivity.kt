package app.checkboard_20190709.view.group_mng

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import app.checkboard_20190709.R
import app.checkboard_20190709.db.ElementGroup
import app.checkboard_20190709.db.RealmManager
import app.checkboard_20190709.view.cmn.LinearLayoutManagerWithSmoothScroller
import app.checkboard_20190709.view.cmn.TitleEditDialog
import kotlinx.android.synthetic.main.activity_group_mng.*
import kotlinx.android.synthetic.main.content_list.*
import org.jetbrains.anko.toast

class GroupMngActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_mng)

        // アクションバー(ツールバー).
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.setHomeButtonEnabled(true)
        }

        fab_add_list_item.setOnClickListener {
            showTitleEditDialog(null, -1)
        }

        rv_list.apply{
            setHasFixedSize(true)
            layoutManager = LinearLayoutManagerWithSmoothScroller(context)
            val divider = DividerItemDecoration(
                context, DividerItemDecoration.VERTICAL)
            this.addItemDecoration(divider)
        }

        loadGroupList()
    }

    override fun onBackPressed() {
        disposeGroupList()
        super.onBackPressed()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun showTitleEditDialog(group: ElementGroup?, position: Int) {

        val dialog = TitleEditDialog()

        dialog.setOnClickLister(
            object: TitleEditDialog.OnClickListener {
                override fun onPositiveClick(inputTitle: String) {
                    registGroup(group?.id, inputTitle, position)
                }
                override fun onNegativeClick() {}
            }
        )

        dialog.arguments = Bundle().apply {
            group?.let {
                this.putCharSequence("arg_title", it.title )
            }
        }

        dialog.show(supportFragmentManager, "dialog")
    }

    /**
     * グループリスト ロード.
     */
    private fun loadGroupList() {

        disposeGroupList()

        val groups = RealmManager.selectElementGroups()
        val adapter = GroupListAdapter(
            groups.toMutableList(), supportFragmentManager)

        adapter.setEventListener(
            object : GroupListAdapter.GroupListEventListener {

                override fun onGroupSwiped(groupId: Long, position: Int) {
                    deleteGroup(groupId, position)
                }

                override fun onItemClick(position: Int, item: ElementGroup) {
                    showTitleEditDialog(item, position)
                }

                override fun dispSeqUpdated() { }
            }
        )

        rv_list.apply{
            this.adapter = adapter
        }
    }

    /**
     * DB登録　グループ
     */
    private fun registGroup(
        groupdId: Long?, inputTitle: String, position: Int) {
        if (inputTitle == "") return
        val registedGroup = RealmManager.
            registElementGroup(groupdId, inputTitle)

        val isNewData = (groupdId == null)
        val adapter = rv_list.adapter as GroupListAdapter

        if (isNewData) {
            adapter.dataset.add(registedGroup)
            val insertIndex = adapter.itemCount - 1
            adapter.notifyItemInserted(insertIndex)
            rv_list.smoothScrollToPosition(insertIndex)
        }
        else {
            adapter.dataset[position] = registedGroup
            rv_list.adapter.notifyItemChanged(position)
        }

        toast(if (isNewData) "追加しました" else "修正しました")
    }

    /**
     * DB削除　グループ
     */
    private fun deleteGroup(groupId: Long, position: Int) {

        RealmManager.deleteElementGroup(groupId)

        val adapter = rv_list.adapter as GroupListAdapter

        adapter.dataset.removeAt(position)
        adapter.notifyItemRemoved(position)

        toast("削除しました")
    }

    private fun disposeGroupList() {
        rv_list?.adapter?.let {
            (it as GroupListAdapter).dispose()
        }
    }
}
