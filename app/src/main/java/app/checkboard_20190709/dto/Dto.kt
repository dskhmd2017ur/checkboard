package app.checkboard_20190709.dto

import app.checkboard_20190709.view.cmn.ElementGroupItem


class ReportItemInputData {
    var title: String = ""
    var qty: String = ""
    var actualQty: String = ""
    var memo: String = ""

    val qtyValue: Float
        get() {
            return this.qty.toFloatOrNull() ?: 0F
        }

    val actualQtyValue: Float
        get() {
            return this.actualQty.toFloatOrNull() ?: 0F
        }
}

class TitleAndGroupInputData {

    var title: String = ""
    var groupItem: ElementGroupItem? = null
    var newGroupTitle: String = ""
}